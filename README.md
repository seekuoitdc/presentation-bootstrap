SEEK Bootstrap Presentation
===========================

ToDo List
---------
- Add column offsets to grid system chapter
- Link to relevant existing Bootstrap documentation
- Add chapters for more components
  - Buttons, Alerts
  - List groups
  - Forms
  - Collapse, Dropdowns
  - Navbar
  - Cards (new in Bootstrap 4)
- Create a chapter for the Bootstrap Starter Template
