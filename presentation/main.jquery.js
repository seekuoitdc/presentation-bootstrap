$( document ).ready(function () {

	// === Code that makes code previews work === //
	$('.pres-card-code').each(function ()  {

		// Get required components
		var code =      $(this).find('textarea')             .first();
		var tryButton = $(this).find('.btn[data-action=try]').first();

		// Handle "Try Code" button click
		tryButton.click(function () {
			var previewModal = $('#previewModal');
			var previewFrame = previewModal.find('iframe').first();

			if (code.data('use-bootstrap')==true) {
				// Set HTML for iframe
				previewFrame.contents().find('html').load(
					'./presentation/bstemplate.html',
					function () {
						previewFrame.contents().find('#contents').html(code.val());
					}
				);

			} else {
				// Set HTML for iframe
				previewFrame.contents().find('html').html(code.val());
			}

			// Show the modal box
			previewModal.modal('show');
		});

	});

	// === Code that makes anchors have animated scrolling === //
	$("a[href^='#']").click(function () {
		// Get named anchor location
		var aLink = $(this);
		var q = 'a[name='+aLink.attr("href").substr(1)+']';
		// If anybody can figure out why I need to add window.scrollTop when
		// normally it's not needed I will buy them a whole box of cookies.
		var relativeTop = $(q).offset().top + $(window).scrollTop();
		// Scroll to the named anchor
		$(document.body).animate({
			'scrollTop': relativeTop
		}, 500);
	});
});


function load(pagename) {
	var scriptTag = document.scripts[document.scripts.length - 1];
	var parentTag = scriptTag.parentNode;
	$(parentTag).load(pagename);
}
